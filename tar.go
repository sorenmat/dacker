package main

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

type entry struct {
	filename        string
	destinationpath string
}

func addFile(tw *tar.Writer, dir string, path entry) error {
	fmt.Println("Trying to add: ", path.filename)
	file, err := os.Open(path.filename)
	if err != nil {
		return err
	}
	defer file.Close()
	if stat, err := file.Stat(); err == nil {
		header, _ := tar.FileInfoHeader(stat, "")
		header.Name = path.destinationpath
		// write the header to the tarball archive
		if err := tw.WriteHeader(header); err != nil {
			return err
		}
		// copy the file data to the tarball
		if _, err := io.Copy(tw, file); err != nil {
			return err
		}
	}
	return nil
}

func NewTarGz(destinationdir string, name string, paths []entry) {
	ext := ".tar.gz"
	// set up the output file

	file, err := os.Create(filepath.Join(destinationdir, name+ext))
	if err != nil {
		log.Fatalln("Unable to create tar.gz file", err)
	}

	defer file.Close()
	// set up the gzip writer
	gw := gzip.NewWriter(file)
	defer gw.Close()
	tw := tar.NewWriter(gw)
	defer tw.Close()

	// add each file as needed into the current tar archive
	for i := range paths {
		if err := addFile(tw, destinationdir, paths[i]); err != nil {
			log.Fatalf("Unable to add path to archive %v got error %v\n", paths[i], err)
		}
	}
}
