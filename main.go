package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/alecthomas/kingpin"
	"github.com/blakesmith/ar"
)

var app = kingpin.New("dacker", "Debian packer")

var version = app.Flag("version", "Version of the package").Default("0.1").String()
var arch = app.Flag("arch", "Architecture of the binary").Default("amd64").String()
var service = app.Flag("service", "Is the binary a service that needs to be started, this will include init script").Bool()
var arguments = app.Flag("service_tags", "Arguments to the service script").String()

var maintainers = app.Flag("maintainers", "Maintainers of the package <John Doe> john@doe.net").String()
var description = app.Flag("description", "Description of the package").String()
var binary = app.Arg("binary", "Binary to include in package").Required().String()

// Creates the debian-binary file, which is the file indication
// what debian package version is used.
func debianbinary(dir string) {
	f, err := os.Create(filepath.Join(dir, "debian-binary"))
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	fmt.Fprintf(f, "2.0")

}

func createServiceScript(dir string) {
	f, err := os.Create(filepath.Join(dir, *binary+".service"))
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	// Mandatory fields
	fmt.Fprintf(f, "[Unit]\n")
	fmt.Fprintf(f, "Description=%v\n", *description)
	fmt.Fprintf(f, "[Service]\n")
	fmt.Fprintf(f, "Type=forking\n")
	fmt.Fprintf(f, "ExecStart=/usr/local/bin/%v %v\n", *binary, *arguments)
	fmt.Fprintf(f, "[Install]\n")
	fmt.Fprintf(f, "WantedBy=multi-user.target\n")
}

func createUpstartScript(dir string) {
	f, err := os.Create(filepath.Join(dir, *binary+".conf"))
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	// Mandatory fields
	fmt.Fprintf(f, "description \"Job that runs the %v daemon\"\n", *binary)

	fmt.Fprintf(f, "# start in normal runlevels when disks are mounted and networking is available\n")
	fmt.Fprintf(f, "start on runlevel [2345]\n")

	fmt.Fprintf(f, "# stop on shutdown/halt, single-user mode and reboot\n")
	fmt.Fprintf(f, "stop on runlevel [016]\n")

	fmt.Fprintf(f, "# create a directory needed by the daemon\n")
	fmt.Fprintf(f, "exec /usr/local/bin/%v %v\n", *binary, *arguments)
}

// This file contains important metadata about the package
func control(dir string, packageName string) {
	f, err := os.Create(filepath.Join(dir, "control"))
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	// Mandatory fields
	fmt.Fprintf(f, "Package: %v\n", packageName)
	fmt.Fprintf(f, "Architecture: %v\n", *arch)

	// recommended fields
	fmt.Fprintf(f, "Maintainer: %v\n", *maintainers)
	fmt.Fprintf(f, "Standards-Version: 3.9.8\n")
	fmt.Fprintf(f, "Version: %v\n", *version)
	fmt.Fprintf(f, "Description: %v\n", *description)

	NewTarGz(dir, "control", []entry{
		entry{
			filename:        filepath.Join(dir, "control"),
			destinationpath: "control",
		},
	})

}

func main() {
	kingpin.MustParse(app.Parse(os.Args[1:]))

	dir, err := ioutil.TempDir("", "test")
	defer os.RemoveAll(dir)

	if err != nil {
		log.Fatal(err)
	}

	err = os.Mkdir(dir+"/debian", 0755)
	if err != nil {
		log.Fatal(err)
	}

	control(dir, *binary)
	debianbinary(dir)
	createServiceScript(dir)
	createUpstartScript(dir)

	NewTarGz(dir, "data", []entry{
		entry{
			filename:        *binary,
			destinationpath: filepath.Join("/usr/local/bin", *binary),
		},
		entry{
			filename:        filepath.Join(dir, *binary+".service"),
			destinationpath: fmt.Sprintf("/lib/systemd/system/%v.service", *binary),
		},
		entry{
			filename:        filepath.Join(dir, *binary+".conf"),
			destinationpath: fmt.Sprintf("/etc/init/%v.conf", *binary),
		},
	})
	debPackage := fmt.Sprintf("%v_%v_%v%v", *binary, *version, *arch, ".deb")
	fd, err := os.OpenFile(debPackage, os.O_RDWR|os.O_TRUNC|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal("Unable to open file: ", err)
	}

	hdr := new(ar.Header)
	body := "2.0\n"
	hdr.ModTime = time.Unix(1361157466, 0)
	hdr.Name = "debian-binary"
	hdr.Size = int64(len(body))
	hdr.Mode = 0644
	hdr.Uid = 501
	hdr.Gid = 20

	//	var buf bytes.Buffer
	writer := ar.NewWriter(fd)
	writer.WriteGlobalHeader()
	writer.WriteHeader(hdr)
	_, err = writer.Write([]byte(body))
	if err != nil {
		log.Fatalf(err.Error())
	}

	addFileToDeb(writer, "control.tar.gz", filepath.Join(dir, "control.tar.gz"))
	addFileToDeb(writer, "data.tar.gz", filepath.Join(dir, "data.tar.gz"))
	f, _ := os.Open(*binary)
	defer f.Close()

}

func addFileToDeb(writer *ar.Writer, path string, file string) {
	f, _ := os.Open(file)
	stat, _ := os.Stat(file)

	hdr := new(ar.Header)
	hdr.ModTime = time.Unix(1361157466, 0)
	hdr.Name = path
	hdr.Size = stat.Size()
	hdr.Mode = 0644
	hdr.Uid = 501
	hdr.Gid = 20
	body, _ := ioutil.ReadAll(f)
	writer.WriteHeader(hdr)
	_, err := writer.Write(body)
	if err != nil {
		log.Fatalf(err.Error())
	}

}
