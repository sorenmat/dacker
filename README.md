# Dacker - The simple binary debian packer

Dacker is a small utility to create debian packages from Go binaries.


```
usage: dacker [<flags>] <binary>

Debian packer

Flags:
  --help                     Show context-sensitive help (also try --help-long and --help-man).
  --version="0.1"            Version of the package
  --arch="amd64"             Architecture of the binary
  --maintainers=MAINTAINERS  Maintainers of the package <John Doe> john@doe.net
  --description=DESCRIPTION  Description of the package

Args:
  <binary>  Binary to include in package
```

